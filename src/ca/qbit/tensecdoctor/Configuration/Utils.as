package ca.qbit.tensecdoctor.Configuration
{
	import ca.qbit.tensecdoctor.Data.Registry;
	import org.flixel.FlxSave;
		
	/**
	 * Various utility functions
	 * @author nospoone
	 */
	public class Utils
	{
		/**
		 * Removes all spaces from a string.
		 */
		public static function stripSpaces(theString:String):String {
			// Split the string at every space
			var wordArray:Array = theString.split(' ');
			
			// Return the joined array
			return(wordArray.join(''));
		}
		
		/**
		 * Returns random number between min and max.
		 */
		public static function randomBetween(min:int = 0, max:int = 1):int {
			return Math.round(min + (max - min) * Math.random());
		}
		
		public static function round2(num:Number, decimals:int):Number
		{
			var m:int = Math.pow(10, decimals);
			return Math.round(num * m) / m;
		}
	}
}
