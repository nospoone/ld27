package ca.qbit.tensecdoctor.Configuration {
	
	/**
	 * Config things
	 * @author nospoone
	 */
	public class Config {
		
		// Animations
		public static var TimeBeforeSplashFadeIn:Number = 1; // In seconds
		public static var TimeBeforeSplashFadeOut:Number = 1; // In seconds
		
		// Chances (all expressed in %)
		public static var ComposedName:Number = 20;
		public static var MiddleName:Number = 20;
		
	}

}