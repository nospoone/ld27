package ca.qbit.tensecdoctor.Configuration {
	/**
	 * AZZZZZZEEEEETTTTSSSSLOLZ
	 * @author nospoone
	 */
	public class Assets {
		
		// Splash
		[Embed(source = "../../../../assets/splash/logo.png")] public static var QBitLogo:Class;
		[Embed(source = "../../../../assets/splash/10sec.png")] public static var GameLogo:Class;
		
		// Cursors
		[Embed(source = "../../../../assets/cursors/cursor.png")] public static var MainCursor:Class;
		[Embed(source = "../../../../assets/cursors/cast-on.png")] public static var CastOn:Class;
		[Embed(source = "../../../../assets/cursors/cast-off.png")] public static var CastOff:Class;
		[Embed(source = "../../../../assets/cursors/hand-on.png")] public static var HandOn:Class;
		[Embed(source = "../../../../assets/cursors/hand-off.png")] public static var HandOff:Class;
		[Embed(source = "../../../../assets/cursors/heart-on.png")] public static var HeartOn:Class;
		[Embed(source = "../../../../assets/cursors/heart-off.png")] public static var HeartOff:Class;
		[Embed(source = "../../../../assets/cursors/pills-on.png")] public static var PillsOn:Class;
		[Embed(source = "../../../../assets/cursors/pills-off.png")] public static var PillsOff:Class;
		[Embed(source = "../../../../assets/cursors/scalpel-on.png")] public static var ScalpelOn:Class;
		[Embed(source = "../../../../assets/cursors/scalpel-off.png")] public static var ScalpelOff:Class;
		[Embed(source = "../../../../assets/cursors/seringe-on.png")] public static var SeringeOn:Class;
		[Embed(source = "../../../../assets/cursors/seringe-off.png")] public static var SeringeOff:Class;
		[Embed(source = "../../../../assets/cursors/stetho-on.png")] public static var StethoOn:Class;
		[Embed(source = "../../../../assets/cursors/stetho-off.png")] public static var StethoOff:Class;
		[Embed(source = "../../../../assets/cursors/thermo-on.png")] public static var ThermoOn:Class;
		[Embed(source = "../../../../assets/cursors/thermo-off.png")] public static var ThermoOff:Class;
		[Embed(source = "../../../../assets/cursors/vaccine-on.png")] public static var VaccineOn:Class;
		[Embed(source = "../../../../assets/cursors/vaccine-off.png")] public static var VaccineOff:Class;
		[Embed(source = "../../../../assets/cursors/xray-on.png")] public static var XRayOn:Class;
		[Embed(source = "../../../../assets/cursors/xray-off.png")] public static var XRayOff:Class;
		
		// Sprites
		[Embed(source = "../../../../assets/sprites/symptom-bar.png")] public static var SymptomBar:Class;
		[Embed(source = "../../../../assets/sprites/icons.png")] public static var Buttons:Class;
		[Embed(source = "../../../../assets/sprites/dummy.png")] public static var DummyPatient:Class;
		[Embed(source = "../../../../assets/sprites/patients.png")] public static var Patients:Class;
		[Embed(source = "../../../../assets/sprites/card.png")] public static var Card:Class;
		[Embed(source = "../../../../assets/sprites/bg.png")] public static var Background:Class;
		[Embed(source = "../../../../assets/sprites/Doctor1.png")] public static var Doctor:Class;
		[Embed(source = "../../../../assets/sprites/backgroundno.png")] public static var CutsceneBackground:Class;
		[Embed(source = "../../../../assets/sprites/enter.png")] public static var EnterButton:Class;
		[Embed(source = "../../../../assets/sprites/gameover.png")] public static var GameOver:Class;
		[Embed(source = "../../../../assets/sprites/titlescreen.png")] public static var TitleScreen:Class;
		
		// Tutorial
		[Embed(source = "../../../../assets/sprites/tutorial1.png")] public static var TutorialBG1:Class;
		[Embed(source = "../../../../assets/sprites/tutorial2.png")] public static var TutorialBG2:Class;
		
		// Fonts
		[Embed(source = "../../../../assets/fonts/emulogic.ttf", fontName = "Mono", embedAsCFF = "false")] private static var Mono:Class;
		
		// Music
		[Embed(source = "../../../../assets/music/10sechype.mp3")] public static var HypeMusic:Class;
		[Embed(source = "../../../../assets/music/menu.mp3")] public static var TitleMusic:Class;
		
		// SFX
		[Embed(source = "../../../../assets/sfx/countdown.mp3")] public static var CountdownSFX:Class;
		[Embed(source = "../../../../assets/sfx/fail.mp3")] public static var FailSFX:Class;
		[Embed(source = "../../../../assets/sfx/good.mp3")] public static var GoodSFX:Class;
		[Embed(source = "../../../../assets/sfx/item.mp3")] public static var ItemSFX:Class;
		[Embed(source = "../../../../assets/sfx/start.mp3")] public static var StartSFX:Class;
		
		// Voices In (Female)
		[Embed(source = "../../../../assets/voices/female/In-1.mp3")] private static var FemaleIn1:Class;
		[Embed(source = "../../../../assets/voices/female/In-2.mp3")] private static var FemaleIn2:Class;
		[Embed(source = "../../../../assets/voices/female/In-3.mp3")] private static var FemaleIn3:Class;
		[Embed(source = "../../../../assets/voices/female/In-4.mp3")] private static var FemaleIn4:Class;
		[Embed(source = "../../../../assets/voices/female/In-5.mp3")] private static var FemaleIn5:Class;
		[Embed(source = "../../../../assets/voices/female/In-6.mp3")] private static var FemaleIn6:Class;
		[Embed(source = "../../../../assets/voices/female/In-7.mp3")] private static var FemaleIn7:Class;
		public static var FemaleIn:Array = [FemaleIn1, FemaleIn2, FemaleIn3, FemaleIn4, FemaleIn5, FemaleIn6, FemaleIn7];
		
		// Voices Out (Female)
		[Embed(source = "../../../../assets/voices/female/Out-1.mp3")] private static var FemaleOut1:Class;
		[Embed(source = "../../../../assets/voices/female/Out-2.mp3")] private static var FemaleOut2:Class;
		[Embed(source = "../../../../assets/voices/female/Out-3.mp3")] private static var FemaleOut3:Class;
		[Embed(source = "../../../../assets/voices/female/Out-4.mp3")] private static var FemaleOut4:Class;
		[Embed(source = "../../../../assets/voices/female/Out-5.mp3")] private static var FemaleOut5:Class;
		[Embed(source = "../../../../assets/voices/female/Out-6.mp3")] private static var FemaleOut6:Class;
		[Embed(source = "../../../../assets/voices/female/Out-7.mp3")] private static var FemaleOut7:Class;
		public static var FemaleOut:Array = [FemaleOut1, FemaleOut2, FemaleOut3, FemaleOut4, FemaleOut5, FemaleOut6, FemaleOut7];
		
		// Voices In (Male)
		[Embed(source = "../../../../assets/voices/male/In-1.mp3")] private static var MaleIn1:Class;
		[Embed(source = "../../../../assets/voices/male/In-2.mp3")] private static var MaleIn2:Class;
		[Embed(source = "../../../../assets/voices/male/In-3.mp3")] private static var MaleIn3:Class;
		[Embed(source = "../../../../assets/voices/male/In-4.mp3")] private static var MaleIn4:Class;
		[Embed(source = "../../../../assets/voices/male/In-5.mp3")] private static var MaleIn5:Class;
		[Embed(source = "../../../../assets/voices/male/In-6.mp3")] private static var MaleIn6:Class;
		[Embed(source = "../../../../assets/voices/male/In-7.mp3")] private static var MaleIn7:Class;
		public static var MaleIn:Array = [MaleIn1, MaleIn2, MaleIn3, MaleIn4, MaleIn5, MaleIn6, MaleIn7];
		
		// Voices Out (Male)
		[Embed(source = "../../../../assets/voices/male/Out-1.mp3")] private static var MaleOut1:Class;
		[Embed(source = "../../../../assets/voices/male/Out-2.mp3")] private static var MaleOut2:Class;
		[Embed(source = "../../../../assets/voices/male/Out-3.mp3")] private static var MaleOut3:Class;
		[Embed(source = "../../../../assets/voices/male/Out-4.mp3")] private static var MaleOut4:Class;
		[Embed(source = "../../../../assets/voices/male/Out-5.mp3")] private static var MaleOut5:Class;
		[Embed(source = "../../../../assets/voices/male/Out-6.mp3")] private static var MaleOut6:Class;
		[Embed(source = "../../../../assets/voices/male/Out-7.mp3")] private static var MaleOut7:Class;
		public static var MaleOut:Array = [MaleOut1, MaleOut2, MaleOut3, MaleOut4, MaleOut5, MaleOut6, MaleOut7];
		
		
	}

}