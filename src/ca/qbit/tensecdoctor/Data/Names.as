package ca.qbit.tensecdoctor.Data {
	/**
	 * ...
	 * @author nospoone
	 */
	public class Names {
		
		public static var FirstNamesGirls:Array = ["Claude", "Kristina", "Lauriane", "Rébecca", "Dominique", "Carole", "France", "Caro", "Céline", "Amélie", "Justine", "Jasmine", "Sara", "Patricia", "Annie", "Julie", "Alex", "Alice"];
		public static var FirstNamesGuys:Array = ["Claude", "Roméo", "Maxime", "Dimitrios", "Ghislain", "Gabriel", "Emilio", "Dominique", "Boubi", "Charles", "Fel", "Jee", "Dom", "Jonathan", "Phil", "Gabo", "Raphaël", "Gregory", "Herby", "Pierre", "Nicolas"];
		public static var LastNames:Array = ["Lemieux", "Lefebvre", "Labonté", "Boisjoly", "Lianopoulos", "Desautels", "Garneau", "Jean", "Pizarro", "Dupuis", "Lation", "Haineault", "Ferland", "Sws", "Leduc", "Proteau", "Bouliane", "Talbot", "Allard", "Charles", "Robo", "Bruneau", "Dion", "Chabot"];
		public static var MiddleName:Array = ["Jr.", "Sr."];
		
	}

}