package ca.qbit.tensecdoctor.Data {
	/**
	 * ...
	 * @author nospoone
	 */
	public class Diseases {
		public static var Symptoms:Array = ["Headache", // 0
											"Sweat like a pig", // 1
											"Fever", // 2
											"Stomach cramps", // 3
											"West Nile virus", // 4
											"Rabies", // 5
											"Light bite", // 6
											"High blood sugar", // 7
											"Allergies", // 8
											"Falls asleep everywhere", // 9
											"Bleeds profusely from the mouth", // 10
											"Fractured arm", // 11
											"Leg fracture", // 12
											"Fractured hand", // 13
											"Neck fracture", // 14
											"Dyspnea", // 15
											"Chest pain", // 16
											"Pneumonia", // 17
											"Needles stuck in neck", // 18
											"Measles", // 19
											"Burned face", // 20
											"Appendicitis", // 21
											"Tetanus", // 22
											"Chest feels like it's going to explode", // 23
											"Heart attack", // 24
											"Lump in breasts", // 25
											"Tuberculosis", // 26
											"Plague", // 27
											"Object lodged in throat", // 28
											"Swine flu", // 29
											"Lung cancer", // 30
											];
	}

}