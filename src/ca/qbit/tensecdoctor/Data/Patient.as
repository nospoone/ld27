package ca.qbit.tensecdoctor.Data {
	
	import ca.qbit.tensecdoctor.Configuration.Assets;
	import ca.qbit.tensecdoctor.Configuration.Config;
	import ca.qbit.tensecdoctor.Data.Names;
	import ca.qbit.tensecdoctor.Data.Diseases;
	import ca.qbit.tensecdoctor.Configuration.Utils;
	import ca.qbit.tensecdoctor.Entities.Button;
	import org.flixel.FlxG;
	import org.flixel.FlxSound;
	import org.flixel.FlxSprite;
	
	/**
	 * Class holding the patient data
	 * @author nospoone
	 */
	public class Patient {
		// First name
		private var _firstName:String;
		public function get FirstName():String {
			return _firstName;
		}
		
		// Middle name
		private var _middleName:String;
		public function get MiddleName():String {
			return _middleName;
		}
		
		private var _lastName:String;
		public function get LastName():String {
			return _lastName;
		}
		
		public function get FullName():String {
			if (MiddleName != null) {
				return FirstName + " " + MiddleName + " " + LastName;
			} else {
				return FirstName + " " + LastName;
			}
		}
		
		private var _sex:int;
		public function get Sex():int {
			return _sex;
		}
		
		public function get SexText():String {
			switch (_sex) {
				case FEMALE:
					return "Female";
					break;
				case MALE:
					return "Male";
					break;
				case TRANS:
					return "Trans";
					break;
				default:
					return "Female";
					break;
			}
		}
		
		public static const FEMALE:int = 0;
		public static const MALE:int = 1;
		public static const TRANS:int = 2;
		
		private var _age:int;
		public function get Age():int {
			return _age;
		}
		
		private var _voiceID:uint = Utils.randomBetween(0, 6);
		
		private var _voiceIn:FlxSound = new FlxSound();
		public function get VoiceIn():FlxSound {
			return _voiceIn;
		}
		
		private var _voiceOut:FlxSound = new FlxSound();
		public function get VoiceOut():FlxSound {
			return _voiceOut;
		}
		
		private var _disease:String;
		private var _diseaseId:int;
		public function get Disease():String {
			return _disease;
		}
		
		public function get DiseaseID():int {
			return _diseaseId;
		}
		
		private var _appearance:FlxSprite;
		public function get Appearance():FlxSprite {
			return _appearance;
		}
		
		private var _diagZone:uint;
		private var _treatZone:uint;
		
		public function get DiagnosticZone():uint {
			return _diagZone;
		}
		
		public function get TreatmentZone():uint {
			return _treatZone;
		}
		
		private var _diagTool:uint;
		private var _treatTool:uint;
		
		public function get DiagnosticTool():uint {
			return _diagTool;
		}
		
		public function get TreatmentTool():uint {
			return _treatTool;
		}
		
		// Diag and treat places
		public static var ALL:uint = 0;
		public static var MOUTH:uint = 1;
		public static var ARMS:uint = 2;
		public static var LEGS:uint = 3;
		public static var HANDS:uint = 4;
		public static var NECK:uint = 5;
		public static var CHEST:uint = 6;
		public static var HEAD:uint = 7;
		
		public function Patient() {
			// Random age
			var ageRandomizer:int = Utils.randomBetween(1, 100);
			if (ageRandomizer <= 25) {
				_age = Utils.randomBetween(10, 20);
			} else if (ageRandomizer > 25 && ageRandomizer <= 45) {
				_age = Utils.randomBetween(21, 40);
			} else if (ageRandomizer > 45 && ageRandomizer <= 70) {
				_age = Utils.randomBetween(41, 55);
			} else if (ageRandomizer > 70 && ageRandomizer <= 85) {
				_age = Utils.randomBetween(56, 75);
			} else if (ageRandomizer > 70 && ageRandomizer <= 85) {
				_age = Utils.randomBetween(56, 75);
			} else if (ageRandomizer > 85 && ageRandomizer <= 95) {
				_age = Utils.randomBetween(76, 90);
			} else if (ageRandomizer > 95 && ageRandomizer <= 100) {
				_age = Utils.randomBetween(91, 100);
			}
			
			// Random sex
			_sex = Utils.randomBetween(0, 2);
			
			// Random name
			if (_sex == FEMALE) {
				_firstName =  Names.FirstNamesGirls[Utils.randomBetween(0, Names.FirstNamesGirls.length - 1)];
				if (Utils.randomBetween(0, 100) <= Config.ComposedName) {
					_firstName += "-" + Names.FirstNamesGirls[Utils.randomBetween(0, Names.FirstNamesGirls.length - 1)];
				}
			} else if (_sex == MALE) {
				_firstName = Names.FirstNamesGuys[Utils.randomBetween(0, Names.FirstNamesGuys.length - 1)];
				if (Utils.randomBetween(0, 100) <= Config.ComposedName) {
					_firstName += "-" + Names.FirstNamesGuys[Utils.randomBetween(0, Names.FirstNamesGuys.length - 1)];
				}
			} else {
				if (Utils.randomBetween(0, 100) >= 50) {
					_firstName = Names.FirstNamesGuys[Utils.randomBetween(0, Names.FirstNamesGuys.length - 1)] + "-" + Names.FirstNamesGirls[Utils.randomBetween(0, Names.FirstNamesGirls.length - 1)];
				} else {
					_firstName = Names.FirstNamesGirls[Utils.randomBetween(0, Names.FirstNamesGirls.length - 1)] + "-" + Names.FirstNamesGuys[Utils.randomBetween(0, Names.FirstNamesGuys.length - 1)];
				}
			}
			
			if (Utils.randomBetween(0, 100) <= Config.MiddleName) {
				_middleName = Names.MiddleName[Utils.randomBetween(0, Names.MiddleName.length - 1)];
			}
			
			_lastName = Names.LastNames[Utils.randomBetween(0, Names.LastNames.length - 1)];
			
			// Random disease
			while (!_disease) {
				// Generate a temporary one
				_diseaseId = Utils.randomBetween(0, Diseases.Symptoms.length - 1);
				// 18+ check
				if (Age < 18 && _diseaseId > 16) {
					_diseaseId = Utils.randomBetween(0, Diseases.Symptoms.length - 1);
				// Assignment
				} else {
					_disease = Diseases.Symptoms[_diseaseId];
				}
			}
			
			// Set appearance and voice
			_appearance = new FlxSprite(FlxG.width / 2 - 132, FlxG.height / 2 - 47);
			_appearance.loadGraphic(Assets.Patients, true, false, 264, 95);
			_appearance.addAnimation("0", [0], 0, false);
			_appearance.addAnimation("1", [1], 0, false);
			_appearance.addAnimation("2", [2], 0, false);
			_appearance.addAnimation("3", [3], 0, false);
			_appearance.addAnimation("4", [4], 0, false);
			_appearance.addAnimation("5", [5], 0, false);
			
			if (_sex == FEMALE) {
				_appearance.play(String(Utils.randomBetween(2, 5)));
				_voiceIn.loadEmbedded(Assets.FemaleIn[_voiceID]);
				_voiceOut.loadEmbedded(Assets.FemaleOut[_voiceID]);
			} else if (_sex == MALE) {
				_appearance.play(String(Utils.randomBetween(0, 1)));
				_voiceIn.loadEmbedded(Assets.MaleIn[_voiceID]);
				_voiceOut.loadEmbedded(Assets.MaleOut[_voiceID]);
			} else {
				if (Utils.randomBetween(0, 1) == 1) {
					_appearance.play(String(Utils.randomBetween(2, 5)));
					_voiceIn.loadEmbedded(Assets.FemaleIn[_voiceID]);
					_voiceOut.loadEmbedded(Assets.FemaleOut[_voiceID]);
				} else {
					_appearance.play(String(Utils.randomBetween(0, 1)));
					_voiceIn.loadEmbedded(Assets.MaleIn[_voiceID]);
					_voiceOut.loadEmbedded(Assets.MaleOut[_voiceID]);
				}
			}
			
			// Set zones
			switch (DiseaseID) {
				case 0:
				case 1:
				case 2:
					_diagTool = Button.THERMOMETER;
					_diagZone = MOUTH;
					_treatTool = Button.PILLS;
					_treatZone = ALL;
					break;
				case 3:
				case 4:
				case 5:
				case 6:
					_diagTool = Button.SERINGE;
					_diagZone = ARMS;
					_treatTool = Button.PILLS;
					_treatZone = ALL;
					break;
				case 7:
				case 8:
				case 9:
					_diagTool = Button.SERINGE;
					_diagZone = ARMS;
					_treatTool = Button.VACCINE;
					_treatZone = ARMS;
					break;
				case 10:
					_diagTool = Button.HAND;
					_diagZone = ARMS;
					_treatTool = Button.CAST;
					_treatZone = ARMS;
					break;
				case 11:
					_diagTool = Button.HAND;
					_diagZone = LEGS;
					_treatTool = Button.CAST;
					_treatZone = LEGS;
					break;
				case 12:
					_diagTool = Button.HAND;
					_diagZone = HANDS;
					_treatTool = Button.CAST;
					_treatZone = HANDS;
					break;
				case 13:
					_diagTool = Button.HAND;
					_diagZone = NECK;
					_treatTool = Button.CAST;
					_treatZone = NECK;
					break;
				case 14:
					_diagTool = Button.STETHOSCOPE;
					_diagZone = CHEST;
					_treatTool = Button.TRANSPLANT;
					_treatZone = CHEST;
					break;
				case 15:
				case 16:
					_diagTool = Button.XRAY;
					_diagZone = NECK;
					_treatTool = Button.SCALPEL;
					_treatZone = NECK;
					break;
				default:
					break;
			}
		}
		
	}

}