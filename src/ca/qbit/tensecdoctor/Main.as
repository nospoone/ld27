package ca.qbit.tensecdoctor {
	
	import ca.qbit.tensecdoctor.Configuration.Assets;
	import ca.qbit.tensecdoctor.Data.Registry;
	import ca.qbit.tensecdoctor.States.CreditsState;
	import ca.qbit.tensecdoctor.States.CutsceneState;
	import ca.qbit.tensecdoctor.States.GameOver;
	import ca.qbit.tensecdoctor.States.SplashState;
	import ca.qbit.tensecdoctor.States.PlayState;
	import ca.qbit.tensecdoctor.States.StatsState;
	import ca.qbit.tensecdoctor.States.TitleScreen;
	import flash.ui.Mouse;
	import org.flixel.FlxSave;
	
	import org.flixel.FlxG;
	import org.flixel.FlxGame;
	
	/**
	 * 10 Second Doctor
	 * @author nospoone
	 */
	public class Main extends FlxGame {
		[SWF(width = "1000", height = "500", background = "#000000")]
		[Frame(factoryClass="Preloader")]
		public function Main():void {
			super(500, 250, TitleScreen, 2);
			
			Mouse.hide();
			FlxG.mouse.load(Assets.MainCursor, 2);
			
			FlxG.framerate = 60;
			FlxG.flashFramerate = 60;
		}
	}

}