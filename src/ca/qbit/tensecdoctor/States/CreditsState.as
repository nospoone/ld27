package ca.qbit.tensecdoctor.States {
	import org.flixel.FlxButton;
	import org.flixel.FlxG;
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	
	/**
	 * ...
	 * @author nospoone
	 */
	public class CreditsState extends FlxState {
		
		private var _creditsText:FlxText;
		private var _credits2Text:FlxText;
		private var _credits3Text:FlxText;
		private var _credits4Text:FlxText;
		private var _credits5Text:FlxText;
		private var _credits6Text:FlxText;
		
		private var _titleScreen:FlxButton;
		
		public function CreditsState() {}
		
		override public function create():void {
			_creditsText = new FlxText(0, 10, FlxG.width, "Art, SFX, Disease Research");
			_credits2Text = new FlxText(0, 30, FlxG.width, "moshiz");
			
			_credits3Text = new FlxText(0, 70, FlxG.width, "Programming, Music, Additional Art");
			_credits4Text = new FlxText(0, 90, FlxG.width, "nospoone");
			
			_credits5Text = new FlxText(0, 130, FlxG.width, "Additional Disease Research");
			_credits6Text = new FlxText(0, 150, FlxG.width, "Tina-Roarr");
			
			_creditsText.setFormat(null, 16, 0x00a2ff, "center");
			_credits3Text.setFormat(null, 16, 0x00a2ff, "center");
			_credits5Text.setFormat(null, 16, 0x00a2ff, "center");
			
			_credits2Text.setFormat(null, 16, 0xffffff, "center");
			_credits4Text.setFormat(null, 16, 0xffffff, "center");
			_credits6Text.setFormat(null, 16, 0xffffff, "center");
			
			add(_creditsText);
			add(_credits2Text);
			add(_credits3Text);
			add(_credits4Text);
			add(_credits5Text);
			add(_credits6Text);
			
			_titleScreen = new FlxButton(FlxG.width / 2, FlxG.height - 50, "TITLE SCREEN", goBack)
			_titleScreen.x -= _titleScreen.width / 2;
			add(_titleScreen);
			
			super.create();
		}
		
		private function goBack():void {
			FlxG.switchState(new TitleScreen);
		}
		
	}

}