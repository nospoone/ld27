package ca.qbit.tensecdoctor.States {
	import ca.qbit.tensecdoctor.Data.Registry;
	import org.flixel.FlxButton;
	import org.flixel.FlxG;
	import org.flixel.FlxSave;
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	
	/**
	 * ...
	 * @author nospoone
	 */
	public class StatsState extends FlxState {
		
		private var _startButton:FlxButton;
		private var _clearButton:FlxButton;
		private var _killText:FlxText;
		private var _statsText:FlxText;
		private var _save:FlxSave;
		private var _loaded:Boolean;
		private var _stats2Text:FlxText;
		private var _stats3Text:FlxText;
		private var _stats4Text:FlxText;
		
		public function StatsState() {}
		
		override public function create():void {
			load();
			
			_startButton = new FlxButton(FlxG.width / 2, 160, "GO BACK", goBack);
			_startButton.x -= _startButton.width / 2;
			add(_startButton);
			
			_clearButton = new FlxButton(FlxG.width / 2, 160, "CLEAR STATS", clearStats);
			_clearButton.x -= _clearButton.width / 2;
			_clearButton.y += _clearButton.height + 5;
			add(_clearButton);
			
			_killText = new FlxText(0, 15, FlxG.width, "Total cured patients: " + Registry.AllPatientsCured);
			_killText.alignment = 'center';
			_killText.size = 16;
			add(_killText);
			
			_statsText = new FlxText(0, 40, FlxG.width, "Total killed patients: " + Registry.PatientsKilled);
			_statsText.alignment = 'center';
			_statsText.size = 16;
			add(_statsText);
			
			_stats2Text = new FlxText(0, 65, FlxG.width, "Best cure streak: " + Registry.StreakPatientsCured);
			_stats2Text.alignment = 'center';
			_stats2Text.size = 16;
			add(_stats2Text);
			
			_stats4Text = new FlxText(0, 90, FlxG.width, "Total patients raped: " + Registry.StreakPatientsCured);
			_stats4Text.alignment = 'center';
			_stats4Text.size = 16;
			add(_stats4Text);
			
			_stats3Text = new FlxText(0, 115, FlxG.width, "Best time: " + Registry.BestTime);
			if (Registry.BestTime == 11) {
				_stats3Text.text = "Best time: N/A";
			}
			_stats3Text.alignment = 'center';
			_stats3Text.size = 16;
			add(_stats3Text);
		}
		
		private function goBack():void {
			FlxG.switchState(new TitleScreen);
		}
		
		private function clearStats():void {
			_save.data.patients = undefined;
			_save.data.bestTime = undefined;
			_save.data.bestStreak = undefined;
			_save.data.patientsDead = undefined;
		}
		
		public function load():void {
			_save = new FlxSave();
			_loaded = _save.bind("ld2710SecDoctor");
			
			if (_loaded) {
				if (_save.data.patients != undefined) {
					Registry.AllPatientsCured = _save.data.patients;
				} else {
					Registry.AllPatientsCured = 0;
				}
				if (_save.data.bestTime != undefined) {
					Registry.BestTime = _save.data.bestTime;
				} else {
					Registry.BestTime = 11;
				}
				if (_save.data.bestStreak != undefined) {
					Registry.StreakPatientsCured = _save.data.bestStreak;
				} else {
					Registry.StreakPatientsCured = 0;
				}
				if (_save.data.patientsDead != undefined) {
					Registry.PatientsKilled = _save.data.patientsDead;
				} else {
					Registry.PatientsKilled = 0;
				}
			}
			
			FlxG.log(Registry.BestTime);
		}
		
	}

}