package ca.qbit.tensecdoctor.States {
	import ca.qbit.tensecdoctor.Configuration.Assets;
	import ca.qbit.tensecdoctor.Data.Names;
	import ca.qbit.tensecdoctor.Configuration.Utils;
	import ca.qbit.tensecdoctor.Data.Patient;
	import ca.qbit.tensecdoctor.Data.Registry;
	import ca.qbit.tensecdoctor.Entities.Button;
	import com.greensock.TweenMax;
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxPoint;
	import org.flixel.FlxRect;
	import org.flixel.FlxSave;
	import org.flixel.FlxSound;
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	
	/**
	 * ...
	 * @author nospoone
	 */
	public class PlayState extends FlxState {
		
		private var _patientText:FlxText;
		private var _patient:Patient = new Patient();
		
		private var _timer:Number = 10;
		private var _timerText:FlxText;
		
		private var _modeText:FlxText; 
		
		private var _countdownText:FlxText;
		private var _countdownTimer:Number = 1.5;
		
		private var _symptomBar:FlxSprite;
		private var _symptomText:FlxText;
		
		private var _card:FlxSprite;
		
		private var _diagBar:FlxGroup = new FlxGroup();
		private var _treatBar:FlxGroup = new FlxGroup();
		private var _allButtons:FlxGroup = new FlxGroup();
		
		private var _mouseRect:FlxRect = new FlxRect(0, 0, 22, 21);
		private var _mouseRectButtons:FlxRect = new FlxRect(0, 0, 5, 5);
		
		private var _clickZoneDiag1:FlxRect = new FlxRect(0, 0, 0, 0);
		private var _clickZoneTreat1:FlxRect = new FlxRect(0, 0, 0, 0);
		private var _clickZoneDiag2:FlxRect = new FlxRect(0, 0, 0, 0);
		private var _clickZoneTreat2:FlxRect = new FlxRect(0, 0, 0, 0);
		
		private var _rapeZone:FlxRect = new FlxRect(0, 0, 19, 16);
		
		private var _tempGroupDiag:FlxGroup = new FlxGroup();
		private var _tempGroupTreat:FlxGroup = new FlxGroup();
		
		private var _currentTool:Button;
		private var _isHoverDiag:Boolean = false;
		private var _isHoverTreat:Boolean = false;
		private var _patientSprite:FlxSprite;
		private var _clickZoneDiag2Active:Boolean = false;
		private var _requiredDiagTool:Number;
		private var _clickZoneTreat2Active:Boolean = false;
		
		private var _currentAction:int = DIAGNOSTIC;
		
		private static var DIAGNOSTIC:int = 0;
		private static var TREATMENT:int = 1;
		private var _requiredTreatTool:Number;
		private var _hasStarted:Boolean = false;
		
		private static var _bg:FlxSprite;
		
		private var _hypeMusic:FlxSound = new FlxSound().loadEmbedded(Assets.HypeMusic);
		private var _save:FlxSave;
		private var _loaded:Boolean;
		private var _lastFrameText:String = "3";
		private var _isTutorial:Boolean = true;
		
		public function PlayState() {}
		
		override public function create():void {
			
			_rapeZone.x = _patient.Appearance.x + 150;
			_rapeZone.y = _patient.Appearance.y + 39;
			
			load();
			
			_hypeMusic.volume = 0.5;
			
			Registry.CurrentStreakPatientsCured = 0;
			
			FlxG.music.fadeOut(1.0);
			
			_bg = new FlxSprite(0, 0, Assets.Background);
			_bg.alpha = 0;
			add(_bg);
			
			_patientSprite = _patient.Appearance;
			_patientSprite.alpha = 0;
			add(_patientSprite);
			
			_card = new FlxSprite(0, FlxG.height, Assets.Card);
			_card.y -= _card.height;
			add(_card);
			
			_patientText = new FlxText(35, 5, 250);
			_patientText.color = 0xFF000000;
			add(_patientText);
			
			generateButtons();
			shuffleButtons();
			
			_symptomBar = new FlxSprite(FlxG.width, FlxG.height, Assets.SymptomBar);
			_symptomBar.x -= _symptomBar.width;
			add(_symptomBar);
			
			_symptomText = new FlxText(300, FlxG.height, 200, _patient.Disease);
			_symptomText.shadow = 0x66000000;
			add(_symptomText);
			
			_timerText = new FlxText(0, 3, FlxG.width, "10.00");
			_timerText.setFormat("Mono", 16, 0xFFFFFF, "center");
			_timerText.shadow = 0x66000000;
			add(_timerText);
			
			TweenMax.to(_symptomBar, 1, { y: FlxG.height - _symptomBar.height } );
			TweenMax.to(_symptomText, 1, { y: FlxG.height - _symptomText.height } );
			TweenMax.to(_bg, 1, { alpha: 1 } );
			
			generateNewPatient();
			
			_modeText = new FlxText(FlxG.width - 151, FlxG.height - _symptomBar.height - 15, 150, "Diagnostic Mode");
			_modeText.alignment = "right";
			_modeText.shadow = 0x66000000;
			add(_modeText);
			
			_countdownText = new FlxText(0, FlxG.height / 2, FlxG.width, "3");
			_countdownText.setFormat("Mono", 64, 0xFFFFFF, 'center');
			_countdownText.y -= 45;
			_countdownText.shadow = 0x66000000;
			add(_countdownText);
			
			newGame();
			
			super.create();
		}
		
		
		private function shuffleButtons():void {
			_tempGroupDiag = _diagBar;
			_tempGroupTreat = _treatBar;
			var _posArray1:Array = new Array();
			var _posArray2:Array = new Array();
			
			for (var i:int = 0; i < 5; i++) {
				_posArray1.push((25 * i) + (i * 5) + 5);
				_posArray2.push((25 * i) + (i * 5) + 5);
			}
			
			for each (var btn1:Button in _diagBar.members) {
				var currentIndex:uint = Utils.randomBetween(0, _posArray1.length - 1);
				btn1.y = _posArray1[currentIndex];
				btn1.Tooltip.y = _posArray1[currentIndex] + 6;
				TweenMax.to(btn1, 1, { x : 5 } );
				TweenMax.to(btn1.Tooltip, 1, { x : btn1.x + 30 } );
				_posArray1.splice(currentIndex, 1);
			}
			
			for each (var btn2:Button in _treatBar.members) {
				var currentIndex2:uint = Utils.randomBetween(0, _posArray2.length - 1);
				btn2.y = _posArray2[currentIndex];
				btn2.Tooltip.y = _posArray2[currentIndex] + 6;
				_posArray2.splice(currentIndex, 1);
			}
		}
		
		private function treatmentBarAppear():void {
			for each (var btn2:Button in _treatBar.members) {
				TweenMax.to(btn2, 1, { x : FlxG.width - 30 } );
				TweenMax.to(btn2.Tooltip, 1, { x : FlxG.width - btn2.Tooltip.width - 31 } );
			}
		}
		
		private function treatmentBarDisappear():void {
			for each (var btn2:Button in _treatBar.members) {
				TweenMax.to(btn2, 1, { x : FlxG.width } );
				TweenMax.to(btn2.Tooltip, 1, { x : FlxG.width + btn2.Tooltip.width } );
			}
		}
		
		private function generateButtons():void {					
			for (var i:int = 0; i < 5; i++) {
				var diagButton:Button = new Button(i + 1, -25, 0);
				var treatButton:Button = new Button(i + 6, FlxG.width, (25 * i) + (i * 5) + 5);
				
				diagButton.kill();
				treatButton.kill();
				
				_tempGroupDiag.add(diagButton);
				_allButtons.add(diagButton);
				
				_tempGroupTreat.add(treatButton);
				_allButtons.add(treatButton);
			}
			
			for (var j:int = 0; j < 5; j++) {
				var theBtn:Button = (_tempGroupDiag.getRandom() as Button);
				theBtn.y = (25 * j) + (j * 5) + 5;
				_diagBar.add(theBtn);
				_tempGroupDiag.remove(theBtn, true);
				
				var leBtn:Button = (_tempGroupTreat.getRandom() as Button);
				leBtn.y = (25 * j) + (j * 5) + 5;
				_treatBar.add(leBtn);
				_tempGroupTreat.remove(leBtn, true);
			}
			
			for each (var btn1:Button in _diagBar.members) {
				btn1.revive();
				add(btn1);
				btn1.Tooltip.y = btn1.y + 6;
				add(btn1.Tooltip);
				TweenMax.to(btn1, 1, { x : 5 } );
				TweenMax.to(btn1.Tooltip, 1, { x : btn1.Tooltip.x + 30 });
			}
			
			for each (var btn2:Button in _treatBar.members) {
				btn2.revive();
				add(btn2);
				btn2.Tooltip.y = btn2.y + 6;
				add(btn2.Tooltip);
			}
		}
		
		override public function update():void {
			// debug
			if (FlxG.keys.justPressed('A')) {
				newGame();
			}
			
			if (_isTutorial == false) {
				
			}
			
			super.update();
			
			if (_countdownTimer > 1.0) {
				_countdownText.text = "3";
				if (_lastFrameText != _countdownText.text) {
					FlxG.play(Assets.CountdownSFX);
				}
			} else if (_countdownTimer > 0.5 && _countdownTimer <= 1.0) {
				_countdownText.text = "2";
				if (_lastFrameText != _countdownText.text) {
					FlxG.play(Assets.CountdownSFX);
				}
			} else if (_countdownTimer > 0 && _countdownTimer <= 0.5) {
				_countdownText.text = "1";
				if (_lastFrameText != _countdownText.text) {
					FlxG.play(Assets.CountdownSFX);
				}
			} else if (_countdownTimer <= 0 && !_hasStarted) {
				_countdownText.kill();
				_hasStarted = true;
				_hypeMusic.play(true);
				TweenMax.to(_patientSprite, 0.1, { alpha: 1 } );
				_patient.VoiceIn.play();
				FlxG.play(Assets.StartSFX, 0.3);
			}
			
			_lastFrameText = _countdownText.text;
			
			if (_countdownTimer > 0) {
				_countdownTimer -= FlxG.elapsed;
				
			}
			
			if (_hasStarted) {
				if (_timer > 0) {
					_timer -= FlxG.elapsed;
					var string:String = String(Utils.round2(_timer, 2));
					if (string.length == 3) {
						string += "0";
					} else if (string.length == 2) {
						string += "00";
					} else if (string.length == 1) {
						string += ".00"
					}
					
					_timerText.text = "0" + string;
				} else {
					_timerText.text = "00.00";
				}
			}
			
			if (_timer <= 0) {
				Registry.PatientsKilled++;
				Registry.GameOverReason = Registry.KILL;
				save();
				_hypeMusic.stop();
				FlxG.mouse.load(Assets.MainCursor, 2);
				if (_currentTool != null) {
					_currentTool.play(_currentTool.Type.toString());
					_currentTool = null;
				}
				FlxG.switchState(new GameOver());
			}
			
			if (FlxG.mouse.justPressed()) {
				if (_mouseRect.overlaps(_rapeZone) && _currentTool.Type == 1) {
					Registry.PatientsRaped++;
					Registry.GameOverReason = Registry.RAPE;
					save();
					FlxG.mouse.load(Assets.MainCursor, 2);
					if (_currentTool != null) {
						_currentTool.play(_currentTool.Type.toString());
						_currentTool = null;
					}
					_hypeMusic.stop();
					FlxG.switchState(new GameOver());
				}
			}
			
			if (FlxG.keys.justPressed("ESCAPE")) {
				FlxG.mouse.load(Assets.MainCursor, 2);
				if (_currentTool != null) {
					_currentTool.play(_currentTool.Type.toString());
					_currentTool = null;
				}
			}
			
			if (FlxG.mouse.justPressed() && _currentTool != null) {
				FlxG.mouse.load(_currentTool.RelatedCursorDown, 2);
				if (_currentAction == DIAGNOSTIC) {
					if (_mouseRect.overlaps(_clickZoneDiag1) || (_mouseRect.overlaps(_clickZoneDiag2) && _clickZoneDiag2Active)) {
						if (_currentTool.Type == _requiredDiagTool) {
							_currentAction = TREATMENT;
							FlxG.flash();
							FlxG.mouse.load(Assets.MainCursor, 2);
							if (_currentTool != null) {
								_currentTool.play(_currentTool.Type.toString());
								_currentTool = null;
							}
							treatmentBarAppear();
							FlxG.play(Assets.GoodSFX);
						} else {
							FlxG.play(Assets.FailSFX);
						}
					} else {
						FlxG.play(Assets.FailSFX);
					}
				} else if (_currentAction == TREATMENT) {
					if (_mouseRect.overlaps(_clickZoneTreat1) || (_mouseRect.overlaps(_clickZoneTreat2) && _clickZoneTreat2Active)) {
						if (_currentTool.Type == _requiredTreatTool) {
							_hasStarted = false;
							_patient.VoiceOut.play();
							Registry.AllPatientsCured++;
							Registry.CurrentStreakPatientsCured++;
							TweenMax.to(_patientSprite, 0.1, { alpha: 0 } );
							if (Registry.BestTime > 10 - _timer) {
								Registry.BestTime = Utils.round2(10 - _timer, 2);
							}
							treatmentBarDisappear();
							newGame();
							FlxG.mouse.load(Assets.MainCursor, 2);
							if (_currentTool != null) {
								_currentTool.play(_currentTool.Type.toString());
								_currentTool = null;
							} 
							FlxG.play(Assets.GoodSFX);
						} else {
							FlxG.play(Assets.FailSFX);
						}
					} else {
						FlxG.play(Assets.FailSFX);
					}
				}
			}
			
			for each (var btn:Button in _allButtons.members) {
				if (_mouseRectButtons.overlaps(new FlxRect(btn.x, btn.y, btn.width, btn.height))) {
					if (btn.Tooltip.alpha != 1) {
						TweenMax.to(btn.Tooltip, 0.1, { alpha : 1 } );
					}
					
					if (FlxG.mouse.justPressed()) {
						if (_currentTool == btn) {
							btn.play(_currentTool.Type.toString());
							FlxG.mouse.load(Assets.MainCursor, 2);
							_currentTool.play(_currentTool.Type.toString());
							_currentTool = null;
						} else {
							if (_currentTool != null) {
								_currentTool.play(_currentTool.Type.toString());
							}
							_currentTool = btn;
							_currentTool.play('empty');
							FlxG.mouse.load(_currentTool.RelatedCursor, 2);
							FlxG.play(Assets.ItemSFX);
						}
					}
				} else {
					if (btn.Tooltip.alpha != 0) {
						TweenMax.to(btn.Tooltip, 0.1, { alpha : 0 } );
					}
				}
			}
			
			if (FlxG.mouse.justReleased() && _currentTool != null) {
				FlxG.mouse.load(_currentTool.RelatedCursor, 2);
			}
			
			if (_currentAction == DIAGNOSTIC) {
				_modeText.text = "Diagnostic Mode";
			} else {
				_modeText.text = "Treatment Mode";
			}
			
			_mouseRect.x = FlxG.mouse.x;
			_mouseRect.y = FlxG.mouse.y;
			_mouseRectButtons.x = FlxG.mouse.x;
			_mouseRectButtons.y = FlxG.mouse.y;
		}
		
		private function newGame():void {
			save();
			shuffleButtons();
			generateNewPatient();
			_timer = 10;
			_currentAction = DIAGNOSTIC;
			_timerText.text = "10.00";
			_hasStarted = false;
			_hypeMusic.stop();
			remove(_patientSprite);
			_patientSprite = _patient.Appearance;
			_patientSprite.alpha = 0;
			add(_patientSprite);
		}
		
		private function generateNewPatient():void {
			_patient = new Patient();
			
			_patientText.text = _patient.FullName + "\n" + _patient.SexText + "\n" + _patient.Age + " years old";
			_patientText.x = 5;
			_patientText.y = FlxG.height - _patientText.height - 5
			_symptomText.text = _patient.Disease;
				
				// SET DIAG ZONES
				switch (_patient.DiseaseID) {
					case 0:
					case 1:
					case 2:					
					case 19:
					case 20:
					case 29:
						// FACE
						_clickZoneDiag2Active = false;
						
						_clickZoneDiag1.width = 50;
						_clickZoneDiag1.height = 45;
						_clickZoneDiag1.x = _patientSprite.x + 15;
						_clickZoneDiag1.y = _patientSprite.y + 20;
						break;
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 11:
					case 22:
						// ARMS
						_clickZoneDiag2Active = true;
						
						_clickZoneDiag1.width = 80;
						_clickZoneDiag1.height = 20;
						_clickZoneDiag1.x = _patientSprite.x + 74;
						_clickZoneDiag1.y = _patientSprite.y + 2;
						
						_clickZoneDiag2.width = 76;
						_clickZoneDiag2.height = 20;
						_clickZoneDiag2.x = _patientSprite.x + 74;
						_clickZoneDiag2.y = (_patientSprite.y + _patientSprite.height) - 20 - 2;
						break;
					case 12:
						// LEGS
						_clickZoneDiag2Active = true;
						
						_clickZoneDiag1.width = 71;
						_clickZoneDiag1.height = 17;
						_clickZoneDiag1.x = _patientSprite.x + 168;
						_clickZoneDiag1.y = _patientSprite.y + 24;
						
						_clickZoneDiag2.width = 71;
						_clickZoneDiag2.height = 17;
						_clickZoneDiag2.x = _patientSprite.x + 168;
						_clickZoneDiag2.y = _patientSprite.y + 53;
						break;
					case 13:
						// HANDS
						_clickZoneDiag2Active = true;
						
						_clickZoneDiag1.width = 21;
						_clickZoneDiag1.height = 21;
						_clickZoneDiag1.x = _patientSprite.x + 153;
						_clickZoneDiag1.y = _patientSprite.y;
						
						_clickZoneDiag2.width = 21;
						_clickZoneDiag2.height = 21;
						_clickZoneDiag2.x = _patientSprite.x + 148;
						_clickZoneDiag2.y = (_patientSprite.y + _patientSprite.height) - 21;
						break;
					case 14:
					case 18:
					case 27:
					case 28:
						// NECK
						_clickZoneDiag2Active = false;
						
						_clickZoneDiag1.width = 15;
						_clickZoneDiag1.height = 20;
						_clickZoneDiag1.x = _patientSprite.x + 67;
						_clickZoneDiag1.y = _patientSprite.y + 34;
						break;
					case 15:
					case 16:
					case 17:
					case 21:
					case 23:
					case 24:
					case 25:
					case 26:
					case 30:
						// CHEST
						_clickZoneDiag2Active = false;
						
						_clickZoneDiag1.width = 79;
						_clickZoneDiag1.height = 42;
						_clickZoneDiag1.x = _patientSprite.x + 74;
						_clickZoneDiag1.y = _patientSprite.y + 26;
						break;
					default:
						break;
				}
				
				switch (_patient.DiseaseID) {
					case 0:
					case 1:
					case 2:
					case 29:
						_requiredDiagTool = 2;
						break;
					case 3:
					case 8:
					case 9:
					case 4:
					case 5:
					case 7:
					case 22:
						_requiredDiagTool = 3;
						break;
					case 10:
					case 18:
					case 23:
					case 21:
					case 28:
					case 26:
						_requiredDiagTool = 5;
						break;
					case 6:
					case 11:
					case 12:
					case 13:
					case 14:
					case 20:
					case 25:
					case 27:
						_requiredDiagTool = 1;
						break;
					case 15:
					case 16:
					case 17:
					case 24:
					case 30:
						_requiredDiagTool = 4;
						break;
					default:
						break;
				}
				
				switch (_patient.DiseaseID) {
					case 0:
					case 1:
					case 2:
					case 3:
					case 6:
					case 8:
					case 9:
					case 10:
					case 16:
					case 17:
					case 19:
					case 20:
					case 27: 
						// FACE
						_clickZoneTreat2Active = false;
						
						_clickZoneTreat1.width = 50;
						_clickZoneTreat1.height = 45;
						_clickZoneTreat1.x = _patientSprite.x + 15;
						_clickZoneTreat1.y = _patientSprite.y + 20;
						break;
					case 4:
					case 5:
					case 7:
					case 11:
					case 22:
					case 26:
					case 29:
						// ARMS
						_clickZoneTreat2Active = true;
						
						_clickZoneTreat1.width = 80;
						_clickZoneTreat1.height = 20;
						_clickZoneTreat1.x = _patientSprite.x + 74;
						_clickZoneTreat1.y = _patientSprite.y + 2;
						
						_clickZoneTreat2.width = 76;
						_clickZoneTreat2.height = 20;
						_clickZoneTreat2.x = _patientSprite.x + 74;
						_clickZoneTreat2.y = (_patientSprite.y + _patientSprite.height) - 20 - 2;
						break;
					case 12:
						// LEGS
						_clickZoneTreat2Active = true;
						
						_clickZoneTreat1.width = 71;
						_clickZoneTreat1.height = 17;
						_clickZoneTreat1.x = _patientSprite.x + 168;
						_clickZoneTreat1.y = _patientSprite.y + 24;
						
						_clickZoneTreat2.width = 71;
						_clickZoneTreat2.height = 17;
						_clickZoneTreat2.x = _patientSprite.x + 168;
						_clickZoneTreat2.y = _patientSprite.y + 53;
						break;
					case 13:
						// HANDS
						_clickZoneTreat2Active = true;
						
						_clickZoneTreat1.width = 21;
						_clickZoneTreat1.height = 21;
						_clickZoneTreat1.x = _patientSprite.x + 153;
						_clickZoneTreat1.y = _patientSprite.y;
						
						_clickZoneTreat2.width = 21;
						_clickZoneTreat2.height = 21;
						_clickZoneTreat2.x = _patientSprite.x + 148;
						_clickZoneTreat2.y = (_patientSprite.y + _patientSprite.height) - 21;
						break;
					case 14:
					case 14:
					case 28:
						// NECK
						_clickZoneTreat2Active = false;
						
						_clickZoneTreat1.width = 15;
						_clickZoneTreat1.height = 20;
						_clickZoneTreat1.x = _patientSprite.x + 67;
						_clickZoneTreat1.y = _patientSprite.y + 34;
						break;
					case 15:
					case 21:
					case 23:
					case 24:
					case 25:
					case 30:
						// CHEST
						_clickZoneTreat2Active = false;
						
						_clickZoneTreat1.width = 79;
						_clickZoneTreat1.height = 42;
						_clickZoneTreat1.x = _patientSprite.x + 74;
						_clickZoneTreat1.y = _patientSprite.y + 26;
						break;
					default:
						break;
				}
				
				switch (_patient.DiseaseID) {
					case 0:
					case 1:
					case 2:
					case 3:
					case 6:
					case 8:
					case 9:
					case 10:
					case 16:
					case 17:
					case 19:
					case 27: 
						_requiredTreatTool = 6;
						break;
					case 4:
					case 5:
					case 7:
					case 22:
					case 26:
					case 29:
						_requiredTreatTool = 7;
						break;
					case 18:
					case 23:
					case 21:
					case 25:
					case 28:
						_requiredTreatTool = 8;
						break;
					case 11:
					case 12:
					case 13:
					case 14:
						_requiredTreatTool = 9;
						break;
					case 20:
					case 15:
					case 24:
					case 30:
						_requiredTreatTool = 10;
						break;
					default:
						break;
				}
				
				/* PUBIS
				_clickZoneDiagDebug.scale.x = 19;
				_clickZoneDiagDebug.scale.y = 16;
				_clickZoneDiagDebug.setOriginToCorner();
				_clickZoneDiagDebug.x = _patientSprite.x + 150;
				_clickZoneDiagDebug.y = _patientSprite.y + 39;
				*/
		}
		
		public function save():void {
			if (_loaded) {
				_save.data.patients = Registry.AllPatientsCured;
				if (Registry.CurrentStreakPatientsCured > Registry.StreakPatientsCured) {
					Registry.StreakPatientsCured = Registry.CurrentStreakPatientsCured;
				}
				_save.data.bestStreak = Registry.StreakPatientsCured;
				_save.data.bestTime = Registry.BestTime;
				_save.data.patientsDead = Registry.PatientsKilled;
				_save.data.raped = Registry.PatientsRaped;
			}
		}
		
		public function load():void {
			_save = new FlxSave();
			_loaded = _save.bind("ld2710SecDoctor");
			
			if (_loaded) {
				if (_save.data.patients != undefined) {
					Registry.AllPatientsCured = _save.data.patients;
				} else {
					Registry.AllPatientsCured = 0;
				}
				
				if (_save.data.patientsDead != undefined) {
					Registry.PatientsKilled = _save.data.patientsDead;
				} else {
					Registry.PatientsKilled = 0;
				}
				
				if (_save.data.bestTime != undefined && _save.data.bestTime != 11) {
					Registry.BestTime = _save.data.bestTime;
				} else {
					Registry.BestTime = 11;
				}
				if (_save.data.bestStreak != undefined) {
					Registry.StreakPatientsCured = _save.data.bestStreak;
				} else {
					Registry.StreakPatientsCured = 0;
				}
				if (_save.data.raped != undefined) {
					Registry.PatientsRaped = _save.data.raped;
				} else {
					Registry.PatientsRaped = 0;
				}
			}
			
			FlxG.log(Registry.BestTime);
		}
	}

}