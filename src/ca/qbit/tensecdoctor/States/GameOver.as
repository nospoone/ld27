package ca.qbit.tensecdoctor.States {
	import ca.qbit.tensecdoctor.Configuration.Assets;
	import ca.qbit.tensecdoctor.Data.Registry;
	import org.flixel.FlxButton;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	
	/**
	 * ...
	 * @author nospoone
	 */
	public class GameOver extends FlxState {
		
		private var _startButton:FlxButton;
		private var _titleButton:FlxButton;
		private var _killText:FlxText;
		private var _statsText:FlxText;
		private var _bg:FlxSprite;
		
		public function GameOver() {}
		
		override public function create():void {
			_bg = new FlxSprite(0, 0, Assets.GameOver);
			add(_bg);
			
			_titleButton = new FlxButton(FlxG.width, 130, "TITLE SCREEN", titleGame);
			_titleButton.x -= _titleButton.width + 12;
			add(_titleButton);
			
			_startButton = new FlxButton(FlxG.width, 160, "START AGAIN", restartGame);
			_startButton.x -= _startButton.width + 12;
			add(_startButton);
			
			_killText = new FlxText(0, FlxG.height, FlxG.width - 5, "")
			_killText.alignment = 'right';
			_killText.size = 16;
			_killText.color = 0xFFFF0000;
			_killText.shadow = 0x66000000;
			_killText.y -= _killText.height + 20;
			
			if (Registry.GameOverReason == Registry.KILL) {
				if (Registry.CurrentStreakPatientsCured == 0) {
					_killText.text = "You didn't cure any patients before killing one."
				} else if (Registry.CurrentStreakPatientsCured == 1) {
					_killText.text = "You cured only one patient before killing one."
				} else {
					_killText.text = "You cured " + Registry.CurrentStreakPatientsCured + " patients before killing one."
				}
			} else if (Registry.GameOverReason == Registry.RAPE) {
				_killText.text = "You raped your patient!"
			}
			
			add(_killText);
			
			/*
			_statsText = new FlxText(0, 55, FlxG.width, )
			_statsText.alignment = 'center';
			_statsText.size = 16;
			add(_statsText);*/
		}
		
		private function titleGame():void {
			FlxG.switchState(new TitleScreen);
		}
		
		private function restartGame():void {
			FlxG.switchState(new PlayState);
		}
		
	}

}