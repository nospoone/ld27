package ca.qbit.tensecdoctor.States {
	import ca.qbit.tensecdoctor.Configuration.Assets;
	import com.greensock.TweenMax;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	/**
	 * ...
	 * @author nospoone
	 */
	public class CutsceneState extends FlxState {
		private var _doctor:FlxSprite;
		private var _dialogDisplay:FlxText;
		private var _dialogTimer:Number = 0;
		private var _inc:uint = 0;
		private var _dialogText:String;
		private var _bg:FlxSprite;
		private var _enterButton:FlxSprite;
		private var _enterButtonFlashTimerOrSomething:Number = 0;
		
		public function CutsceneState() { }
		
		override public function create():void {
			// Background
			_bg = new FlxSprite(0, 0, Assets.CutsceneBackground);
			_bg.alpha = 0;
			add(_bg);
			TweenMax.to(_bg, 0.5, { alpha: 1 } );
			
			// Doctor + Animation
			_doctor = new FlxSprite(FlxG.width + 300, FlxG.height, Assets.Doctor);
			_doctor.y -= _doctor.height;
			add(_doctor);
			TweenMax.to(_doctor, 0.5, { x: FlxG.width - _doctor.width - 15 } );
			
			// Dialog text
			_dialogDisplay = new FlxText(5, 5, FlxG.width - 10, "");
			_dialogDisplay.shadow = 0x66000000;
			add(_dialogDisplay);
			_dialogText = "Hi, I'm Vaguen Fern and welcome to the Fluterus Memorial Hospital. Due to an overload of patients, we can only allow you 10 seconds to cure your patient. Do your best, bro!";
			
			// Enter button flash
			_enterButton = new FlxSprite(FlxG.width, 25, Assets.EnterButton);
			_enterButton.x -= _enterButton.width + 10;
			add(_enterButton);
			
			super.create();
		}
		
		override public function update():void {
			_dialogDisplay.y = 5;
			_dialogTimer += FlxG.elapsed;
			if (_dialogTimer >= 0.02 && _inc <= _dialogText.length && _doctor.x == FlxG.width - _doctor.width - 15) {
				_dialogDisplay.text += _dialogText.charAt(_inc);
				
				if (_dialogText.charAt(_inc) != " ") {
					_dialogTimer = 0;
				}
				
				_inc++;
			}
			
			if (FlxG.keys.justPressed('ENTER')) {
				FlxG.fade(0xFF000000, 0.5, false, playGame);
			}
			
			if (_enterButtonFlashTimerOrSomething >= 0 && _enterButtonFlashTimerOrSomething < 0.5) {
				_enterButton.alpha = 0;
			} else if (_enterButtonFlashTimerOrSomething >= 0.5 && _enterButtonFlashTimerOrSomething < 1) {
				_enterButton.alpha = 1;
			}
			
			if (_enterButtonFlashTimerOrSomething >= 1) {
				_enterButtonFlashTimerOrSomething = 0;
			} 
			
			_enterButtonFlashTimerOrSomething += FlxG.elapsed;
			
			super.update();
		}
		
		private function playGame():void {
			FlxG.switchState(new PlayState);
		}
		
	}

}