package ca.qbit.tensecdoctor.States {
	
	import ca.qbit.tensecdoctor.Configuration.Assets;
	import ca.qbit.tensecdoctor.Configuration.Config;
	import mx.core.FlexSprite;
	import org.flixel.FlxButton;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	
	/**
	 * ...
	 * @author nospoone
	 */
	public class TitleScreen extends FlxState {
		
		private var _logo:FlxSprite;
		private var _fadeInTimer:Number = 0;
		
		private var _scaleUp:Boolean = true;
		
		private var _startButton:FlxButton;
		private var _statsButton:FlxButton;
		private var _creditsButton:FlxButton;
		
		private var _bg:FlxSprite;
		
		public function TitleScreen() { }
		
		override public function create():void {
			_bg = new FlxSprite(0, 0, Assets.TitleScreen);
			add(_bg);
			
			_logo = new FlxSprite(35, 180, Assets.GameLogo);
			
			if (FlxG.music == null) {
				FlxG.playMusic(Assets.TitleMusic);
			} else {
				FlxG.music.fadeIn(0);
			}
			
			add(_logo);
			
			_startButton = new FlxButton(25, 210, "START", startGame);
			add(_startButton);
			
			_statsButton = new FlxButton(25, 210, "STATS", stats);
			_statsButton.x += _statsButton.width + 10;
			add(_statsButton);
			
			_creditsButton = new FlxButton(25, 210, "CREDITS", goCredits);
			_creditsButton.x += _creditsButton.width * 2 + 20;
			add(_creditsButton);
			
			super.create();
			
			FlxG.flash(0xFF000000, 1);
		}
		
		private function goCredits():void {
			FlxG.switchState(new CreditsState);
		}
		
		private function stats():void {
			FlxG.switchState(new StatsState);
		}
		
		override public function update():void {						
			if (_logo.scale.x >= 1.1 && _scaleUp) {
				_scaleUp = false;
			} else if (_logo.scale.x <= 0.9 && !_scaleUp) {
				_scaleUp = true;
			}
			
			if (_scaleUp) {
				_logo.scale.x += FlxG.elapsed / 4;
				_logo.scale.y += FlxG.elapsed / 4;
			} else {
				_logo.scale.x -= FlxG.elapsed / 4;
				_logo.scale.y -= FlxG.elapsed / 4;
			}
			
			super.update();
		}
		
		private function startGame():void {
			FlxG.switchState(new CutsceneState);
		}
		
	}

}