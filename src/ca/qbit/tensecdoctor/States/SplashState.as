package ca.qbit.tensecdoctor.States {
	
	import ca.qbit.tensecdoctor.Configuration.Assets;
	import ca.qbit.tensecdoctor.Configuration.Config;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	
	/**
	 * Logo splash screen
	 * @author nospoone
	 */
	public class SplashState extends FlxState {
		
		private var _qBitLogo:FlxSprite;
		private var _fadeInTimer:Number = 0;
		private var _fadeOutTimer:Number = 0;
		
		public function SplashState() {}
		
		override public function create():void {
			_qBitLogo = new FlxSprite(FlxG.width / 2, FlxG.height / 2, Assets.QBitLogo);
			_qBitLogo.x -= _qBitLogo.width / 2;
			_qBitLogo.y -= _qBitLogo.height / 2;
			_qBitLogo.alpha = 0;
			
			add(_qBitLogo);
			
			super.create();
		}
		
		override public function update():void {
			if (_fadeInTimer > Config.TimeBeforeSplashFadeIn) {
				if (_qBitLogo.alpha < 1 && _fadeOutTimer < Config.TimeBeforeSplashFadeOut) {
					_qBitLogo.alpha += FlxG.elapsed / 2;
				} else {
					if (_fadeOutTimer > Config.TimeBeforeSplashFadeOut) {
						_qBitLogo.alpha -= FlxG.elapsed / 2;
						if (_qBitLogo.alpha <= 0) {
							FlxG.switchState(new TitleScreen);
						}
					} else {
						_fadeOutTimer += FlxG.elapsed;
					}
				}
			} else {
				_fadeInTimer += FlxG.elapsed;
			}
			
			super.update();
		}
	}

}