package ca.qbit.tensecdoctor.Entities {
	
	import ca.qbit.tensecdoctor.Configuration.Assets;
	import org.flixel.FlxSprite;
	import org.flixel.FlxText;
	
	/**
	 * Button class
	 * @author nospoone
	 */
	public class Button extends FlxSprite {
		
		public static var HAND:int = 1;
		public static var THERMOMETER:int = 2;
		public static var SERINGE:int = 3;
		public static var STETHOSCOPE:int = 4;
		public static var XRAY:int = 5;
		
		public static var PILLS:int = 6;
		public static var VACCINE:int = 7;
		public static var SCALPEL:int = 8;
		public static var CAST:int = 9;
		public static var TRANSPLANT:int = 10;
		
		private var _tooltip:FlxText;
		public function get Tooltip():FlxText {
			return _tooltip;
		}
		
		private var _relatedCursor:Class;
		public function get RelatedCursor():Class {
			return _relatedCursor;
		}
		
		private var _relatedCursorDown:Class;
		public function get RelatedCursorDown():Class {
			return _relatedCursorDown;
		}	
		
		private var _type:int;
		public function get Type():int {
			return _type;
		}		
		
		public function Button(type:int, x:int, y:int) {
			// Load the buttons image
			loadGraphic(Assets.Buttons, true, false, 25, 25);
			
			// Assign its x and y
			this.x = x;
			this.y = y;
			
			// Store type for later ref.
			_type = type;
			
			// Adding animation by type (which equals to frame #... Saves typin'!) + empty
			addAnimation(type.toString(), [type], 0, false);
			addAnimation('empty', [0], 0, false);
			play(type.toString());
			
			_tooltip = new FlxText(x, y + 6, 150);
			_tooltip.size = 8;
			_tooltip.alpha = 0;
			_tooltip.shadow = 0xFF000000;
			switch (type) {
				case HAND:
					_tooltip.text = "Inspect";
					_tooltip.alignment = "left";
					_tooltip.x += width + 2;
					_relatedCursor = Assets.HandOff;
					_relatedCursorDown = Assets.HandOn;
				break;
				case THERMOMETER:
					_tooltip.text = "Thermometer";
					_tooltip.alignment = "left";
					_tooltip.x += width + 2;
					_relatedCursor = Assets.ThermoOff;
					_relatedCursorDown = Assets.ThermoOn;
				break;
				case SERINGE:
					_tooltip.text = "Blood Test";
					_tooltip.alignment = "left";
					_tooltip.x += width + 2;
					_relatedCursor = Assets.SeringeOff;
					_relatedCursorDown = Assets.SeringeOn;
				break;
				case STETHOSCOPE:
					_tooltip.text = "Stethoscope";
					_tooltip.alignment = "left";
					_tooltip.x += width + 2;
					_relatedCursor = Assets.StethoOff;
					_relatedCursorDown = Assets.StethoOn;
				break;
				case XRAY:
					_tooltip.text = "X-Ray";
					_tooltip.alignment = "left";
					_tooltip.x += width + 2;
					_relatedCursor = Assets.XRayOff;
					_relatedCursorDown = Assets.XRayOn;
				break;
				case PILLS:
					_tooltip.text = "Medecine";
					_tooltip.alignment = "right";
					_tooltip.x -= 2;
					_relatedCursor = Assets.PillsOff;
					_relatedCursorDown = Assets.PillsOn;
				break;
				case VACCINE:
					_tooltip.text = "Vaccine";
					_tooltip.alignment = "right";
					_tooltip.x -= 2;
					_relatedCursor = Assets.VaccineOff;
					_relatedCursorDown = Assets.VaccineOn;
				break;
				case SCALPEL:
					_tooltip.text = "Incision";
					_tooltip.alignment = "right";
					_tooltip.x -= 2;
					_relatedCursor = Assets.ScalpelOff;
					_relatedCursorDown = Assets.ScalpelOn;
				break;
				case CAST:
					_tooltip.text = "Put Cast";
					_tooltip.alignment = "right";
					_tooltip.x -= 2;
					_relatedCursor = Assets.CastOff;
					_relatedCursorDown = Assets.CastOn;
				break;
				case TRANSPLANT:
					_tooltip.text = "Transplantation";
					_tooltip.alignment = "right";
					_tooltip.x -= 2;
					_relatedCursor = Assets.HeartOff;
					_relatedCursorDown = Assets.HeartOn;
				break;
				default:
			}
		}
	}

}